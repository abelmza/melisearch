package ar.com.abelmartin.melisearch;

import android.app.Application;

import timber.log.Timber;

public class MeliSearchApplication extends Application {

    @Override public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            // Init timber (log debugging)
            Timber.plant(new Timber.DebugTree() {
                @Override protected String createStackElementTag(StackTraceElement element) {
                    return super.createStackElementTag(element) + "." + element.getMethodName();
                }
            });
        }


    }
}
