package ar.com.abelmartin.melisearch.model.repositories;

import android.arch.lifecycle.Transformations;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import ar.com.abelmartin.melisearch.model.PagedResource;
import ar.com.abelmartin.melisearch.model.remotes.ItemDto;

public class ItemListRepository {

    private final static int PAGE_SIZE = 25;

    public PagedResource<ItemDto> getItemList(String search) {
        ItemListDataSourceFactory dataSourceFactory = new ItemListDataSourceFactory(search);
        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setInitialLoadSizeHint(PAGE_SIZE * 2)
                .setEnablePlaceholders(true)
                .build();

        PagedResource<ItemDto> result = new PagedResource<>();

        result.setStatus(Transformations.switchMap(dataSourceFactory.getDataSource(),
                ItemListDataSource::getStatusHandler
        ));

        result.setPagedList(new LivePagedListBuilder<>(dataSourceFactory, config).build());

        return result;
    }
}
