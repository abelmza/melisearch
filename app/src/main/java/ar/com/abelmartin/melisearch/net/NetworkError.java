package ar.com.abelmartin.melisearch.net;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;

public class NetworkError {
    @Nullable private Throwable cause;

    @NonNull private NetworkErrorType errorType;

    public NetworkError(@Nullable Throwable throwable) {
        if (throwable == null) {
            errorType = NetworkErrorType.UNKNOWN;
        } else {
            try {
                cause = throwable;
                throw throwable;
            } catch (UnknownHostException | ConnectException | SocketTimeoutException | SSLException exception) {
                errorType = NetworkErrorType.NETWORK_UNAVAILABLE;
            } catch (GenericHttpException httpException) {
                errorType = NetworkErrorType.UNKNOWN_SERVER_RESPONSE;
            } catch (Throwable ex) {
                errorType = NetworkErrorType.UNKNOWN;
            }
        }
    }

    public NetworkErrorType getErrorType() {
        return errorType;
    }


    @Override public String toString() {
        return super.toString() + "\n" +
                "Type=" + getErrorType().toString() + "\n" +
                "Cause=" + cause;
    }

    public Throwable getCause() {
        return cause;
    }

    public enum NetworkErrorType {
        NETWORK_UNAVAILABLE,
        UNKNOWN_SERVER_RESPONSE,
        UNKNOWN
    }
}
