package ar.com.abelmartin.melisearch.model.remotes;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ItemDescriptionDto {

    @SerializedName("text")
    private String text;
    @SerializedName("plain_text")
    private String plainText;
    @SerializedName("last_updated")
    private Date lastUpdated;
    @SerializedName("date_created")
    private Date dateCreated;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
