package ar.com.abelmartin.melisearch.net;

import java.io.IOException;

public class GenericHttpException extends IOException {

    public GenericHttpException() {
        super();
    }

    public GenericHttpException(int errorCode) {
        super("Server response: HTTP" + errorCode);
    }
}
