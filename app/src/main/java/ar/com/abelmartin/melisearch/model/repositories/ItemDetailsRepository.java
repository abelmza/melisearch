package ar.com.abelmartin.melisearch.model.repositories;

import android.arch.lifecycle.MutableLiveData;

import ar.com.abelmartin.melisearch.managers.NetworkManager;
import ar.com.abelmartin.melisearch.model.Resource;
import ar.com.abelmartin.melisearch.model.Status;
import ar.com.abelmartin.melisearch.model.remotes.ItemDescriptionDto;
import ar.com.abelmartin.melisearch.model.remotes.ItemDetailsDto;
import ar.com.abelmartin.melisearch.utils.ErrorUtils;

public class ItemDetailsRepository {

    private final MutableLiveData<Resource<ItemDetailsDto>> itemDetails;
    private final MutableLiveData<Resource<ItemDescriptionDto>> itemDescription;

    public ItemDetailsRepository() {
        itemDetails = new MutableLiveData<>();
        itemDescription = new MutableLiveData<>();
    }

    public MutableLiveData<Resource<ItemDetailsDto>> getItemDetails(String itemId) {
        itemDetails.setValue(Resource.initialLoading());
        NetworkManager.getInstance().getItemDetails(itemId, (response, error) -> {
            if (error != null) {
                itemDetails.setValue(Resource.error(ErrorUtils.from(error)));
            } else if (response == null) {
                itemDetails.setValue(Resource.error(Status.ERROR));
            } else {
                itemDetails.setValue(Resource.success(response));
            }
        });
        return itemDetails;
    }

    public MutableLiveData<Resource<ItemDescriptionDto>> getItemDescription(String itemId) {
        itemDescription.setValue(Resource.loading());
        NetworkManager.getInstance().getItemDescription(itemId, (response, error) -> {
            if (error != null) {
                itemDescription.setValue(Resource.error(ErrorUtils.from(error)));
            } else if (response == null) {
                itemDescription.setValue(Resource.error(Status.ERROR));
            } else {
                itemDescription.setValue(Resource.success(response));
            }
        });
        return itemDescription;
    }
}
