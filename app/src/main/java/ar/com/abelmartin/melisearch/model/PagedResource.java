package ar.com.abelmartin.melisearch.model;

import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

/**
 * Wraps PagedLists requests and its status
 * Analog to {@link Resource}, but used for {@link PagedList}
 * @param <T>
 */

public class PagedResource<T> {

    private LiveData<PagedList<T>> pagedList;
    private LiveData<Status> status;

    public LiveData<PagedList<T>> getPagedList() {
        return pagedList;
    }

    public void setPagedList(LiveData<PagedList<T>> pagedList) {
        this.pagedList = pagedList;
    }

    public LiveData<Status> getStatus() {
        return status;
    }

    public void setStatus(LiveData<Status> status) {
        this.status = status;
    }
}
