package ar.com.abelmartin.melisearch.model;

public enum Status {
    SUCCESS,
    INITIAL_LOADING, //used when initial loading logic is different than regular LOADING
    LOADING,
    ERROR, // unknown error
    ERROR_NO_INTERNET, // no internet available
    ERROR_UNEXPECTED_RESPONSE // unexpected server response
}
