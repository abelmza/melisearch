package ar.com.abelmartin.melisearch.utils;

import ar.com.abelmartin.melisearch.model.Status;
import ar.com.abelmartin.melisearch.net.NetworkError;
import timber.log.Timber;

public class ErrorUtils {

    public static Status from(NetworkError error) {
        if (error == null) {
            return Status.ERROR;
        }
        switch (error.getErrorType()) {
            case UNKNOWN_SERVER_RESPONSE:
                return Status.ERROR_UNEXPECTED_RESPONSE;
            case NETWORK_UNAVAILABLE:
                return Status.ERROR_NO_INTERNET;
            default:
                return Status.ERROR;
        }
    }
}
