package ar.com.abelmartin.melisearch.managers;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import ar.com.abelmartin.melisearch.model.remotes.ItemDescriptionDto;
import ar.com.abelmartin.melisearch.model.remotes.ItemDetailsDto;
import ar.com.abelmartin.melisearch.model.remotes.SearchResultsDto;
import ar.com.abelmartin.melisearch.net.HttpErrorInterceptor;
import ar.com.abelmartin.melisearch.net.MeliApiInterface;
import ar.com.abelmartin.melisearch.net.NetworkCallback;
import ar.com.abelmartin.melisearch.net.NetworkError;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Wrapper-singleton class that encapsulates network calls to ML API
 */
public class NetworkManager {
    private final static String BASE_URL = "https://api.mercadolibre.com";

    private static NetworkManager instance;

    private MeliApiInterface apiInterface;

    private NetworkManager() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpErrorInterceptor()) // wraps non successfull HTTPresponses into java exceptions
                .addInterceptor(loggingInterceptor)
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        apiInterface = retrofit.create(MeliApiInterface.class);
    }

    public static NetworkManager getInstance() {
        //TODO avoid this and use dagger
        if (instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

    public void searchItems(String searchQuery, int limit, int offset, NetworkCallback<SearchResultsDto> callback) {
        executeCall(apiInterface.searchItems(searchQuery, limit, offset), callback);
    }

    public void getItemDetails(String itemId, NetworkCallback<ItemDetailsDto> callback) {
        executeCall(apiInterface.getItemDetails(itemId), callback);
    }

    public void getItemDescription(String itemId, NetworkCallback<ItemDescriptionDto> callback) {
        executeCall(apiInterface.getItemDescription(itemId), callback);
    }

    // common call wrapper enqueuer for all separated calls
    private <T> void executeCall(Call<T> call, final NetworkCallback<T> callback) {
        call.enqueue(new Callback<T>() {
            @Override public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                callback.onFinish(response.body(), null);
            }

            @Override public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                callback.onFinish(null, new NetworkError(t));
            }
        });
    }


}
