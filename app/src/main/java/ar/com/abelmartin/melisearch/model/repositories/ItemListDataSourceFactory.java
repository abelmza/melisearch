package ar.com.abelmartin.melisearch.model.repositories;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.paging.DataSource;
import android.support.annotation.Nullable;

import ar.com.abelmartin.melisearch.model.Resource;
import ar.com.abelmartin.melisearch.model.remotes.ItemDto;

public class ItemListDataSourceFactory extends DataSource.Factory<Integer, ItemDto> {

    private String searchQuery;

    // needed for error handling
    private MutableLiveData<ItemListDataSource> dataSource = new MutableLiveData<>();

    public ItemListDataSourceFactory(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    @Override public DataSource<Integer, ItemDto> create() {
        ItemListDataSource itemListDataSource = new ItemListDataSource(searchQuery);
        dataSource.postValue(itemListDataSource);
        return itemListDataSource;
    }

    public MutableLiveData<ItemListDataSource> getDataSource() {
        return dataSource;
    }
}
