package ar.com.abelmartin.melisearch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import ar.com.abelmartin.melisearch.model.Status;
import ar.com.abelmartin.melisearch.model.remotes.ItemDto;
import ar.com.abelmartin.melisearch.model.repositories.ItemListRepository;
import ar.com.abelmartin.melisearch.model.PagedResource;

public class ItemListViewModel extends ViewModel {

    @NonNull private ItemListRepository repository = new ItemListRepository();

    //using transformations to keep observable states
    private MutableLiveData<String> searchString = new MutableLiveData<>();

    private LiveData<PagedResource<ItemDto>> repoResult
            = Transformations.map(searchString, repository::getItemList);

    //unwrapping data so activity can have access to it
    private LiveData<PagedList<ItemDto>> itemListLiveData
            = Transformations.switchMap(repoResult, PagedResource::getPagedList);

    //unwrapping status so activity can have access
    private LiveData<Status> statusLiveData
            = Transformations.switchMap(repoResult, PagedResource::getStatus);


    public @NonNull LiveData<PagedList<ItemDto>> getItemListLiveData() {
        return itemListLiveData;
    }

    public @NonNull LiveData<Status> getStatusLiveData() {
        return statusLiveData;
    }

    public void searchItems(String userSearchString) {
        searchString.setValue(userSearchString);
    }

}
