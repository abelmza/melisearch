package ar.com.abelmartin.melisearch.ui.main;

import android.app.SearchManager;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ar.com.abelmartin.melisearch.R;
import ar.com.abelmartin.melisearch.model.remotes.ItemDto;
import ar.com.abelmartin.melisearch.model.Status;
import ar.com.abelmartin.melisearch.ui.itemDetails.ItemDetailsActivity;
import ar.com.abelmartin.melisearch.utils.ui.BaseActivity;
import ar.com.abelmartin.melisearch.viewmodel.ItemListViewModel;
import butterknife.BindView;
import butterknife.OnClick;

import static ar.com.abelmartin.melisearch.model.Status.LOADING;
import static ar.com.abelmartin.melisearch.model.Status.SUCCESS;

public class MainActivity extends BaseActivity implements ItemListAdapter.ItemSelectedListener, SearchView.OnQueryTextListener {

    @BindView(R.id.rvSearchResults) RecyclerView rvSearchResults;
    @BindView(R.id.tvMessage) TextView tvMessage;
    @BindView(R.id.ivMessage) ImageView ivMessage;

    MenuItem searchMenuItem;// used to expand the searchView
    SearchView searchView;// cant be binded by butterknife because it is not on the view hierarchy

    private ItemListViewModel viewModel;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Activity events
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViewModel();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // User interaction events
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @OnClick({R.id.ivMessage, R.id.tvMessage})
    void onMessageViewsClicked() {
        if (!searchMenuItem.isActionViewExpanded()) {
            searchMenuItem.expandActionView();
        } else {
            searchIfFormIsValid();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void initializeViewModel() {
        viewModel = ViewModelProviders.of(this).get(ItemListViewModel.class);

        viewModel.getItemListLiveData().observe(this, this::showPagedItems);

        viewModel.getStatusLiveData().observe(this, (status -> {
            switch (status) {
                case SUCCESS:
                    showInformationPanel(false);
                    break;
                case LOADING:
                    // dont care, already showing the list.
                    // Dont want this case handled by default
                    break;
                case INITIAL_LOADING:
                    showInformationPanel(true, getString(R.string.please_wait), R.drawable.ic_update_black_150dp);
                    break;
                default:
                    // error handling
                    showInformationPanel(true, getMessageForStatus(status), R.drawable.ic_error_outline_black_100dp);
                    break;
            }
        }));
    }

    private void showPagedItems(PagedList<ItemDto> pagedList) {
        if (rvSearchResults.getLayoutManager() == null) {
            rvSearchResults.setLayoutManager(new LinearLayoutManager(this));
        }

        ItemListAdapter adapter = new ItemListAdapter(this);
        adapter.submitList(pagedList);
        rvSearchResults.setAdapter(adapter);
    }

    /**
     * Validates search input
     *
     * @return true if input is valid
     */
    private boolean inputFormIsValid() {

        if (searchView.getQuery().toString().isEmpty()) {
            getDialogBuilder()
                    .setMessage(R.string.validation_search_string_cant_be_empty)
                    .setCancelable(true)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
            return false;
        }
        return true;
    }

    private void showInformationPanel(boolean show) {
        showInformationPanel(show, null, 0);
    }

    private void showInformationPanel(boolean show, String message, int imageId) {
        if (message != null) {
            tvMessage.setText(message);
        }

        if (imageId != 0) {
            ivMessage.setImageResource(imageId);
        }

        tvMessage.setVisibility(show ? View.VISIBLE : View.GONE);
        ivMessage.setVisibility(show ? View.VISIBLE : View.GONE);
        rvSearchResults.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private boolean searchIfFormIsValid() {
        if (inputFormIsValid()) {
            String query = searchView.getQuery().toString();
            hideSoftKeyboard();
            viewModel.searchItems(query);
            return true;
        } else {

        }
        return false;
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // ItemSelectedListener implementation
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override public void onItemSelected(ItemDto item) {
        startActivity(ItemDetailsActivity.getActivityIntent(this, item.getId()));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // SearchView.OnQueryTextListener implementation
    ////////////////////////////////////////////////////////////////////////////////////////////////


    @Override public boolean onQueryTextSubmit(String query) {
        return searchIfFormIsValid();
    }

    @Override public boolean onQueryTextChange(String newText) {
        return false;
    }
}




