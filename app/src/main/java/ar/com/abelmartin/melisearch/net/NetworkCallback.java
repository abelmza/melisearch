package ar.com.abelmartin.melisearch.net;

import android.support.annotation.Nullable;

public interface NetworkCallback<T> {
    void onFinish(@Nullable T response, @Nullable NetworkError error);
}