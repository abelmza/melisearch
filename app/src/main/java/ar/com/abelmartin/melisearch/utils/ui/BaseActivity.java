package ar.com.abelmartin.melisearch.utils.ui;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import ar.com.abelmartin.melisearch.R;
import ar.com.abelmartin.melisearch.model.Status;
import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity {

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        // Avoid calling butterKnife.bind on each activity
        ButterKnife.bind(this);
    }

    protected AlertDialog.Builder getDialogBuilder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        return builder;
    }

    protected void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showKeyboard(View view) {
        view.requestFocus();
        InputMethodManager imm = (InputMethodManager)   getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    protected String getMessageForStatus(Status status) {
        String message = getString(R.string.error_unknown);
        switch (status) {
            case ERROR_NO_INTERNET:
                message = getString(R.string.error_internet_not_available);
                break;
            case ERROR_UNEXPECTED_RESPONSE:
                message = getString(R.string.error_unknown_server_response);
                break;
        }

        return message;
    }
}
