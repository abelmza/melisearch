package ar.com.abelmartin.melisearch.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Wrapper to hold data and status (used for error handling)
 *
 * @param <T>
 */
public class Resource<T> {

    @NonNull private Status status;
    @Nullable private T data;

    private Resource(@NonNull Status status, @Nullable T data) {
        this.status = status;
        this.data = data;

    }

    public static <T> Resource<T> success(@NonNull T data) {
        return new Resource<T>(Status.SUCCESS, data);
    }

    public static <T> Resource<T> error(Status status) {
        return new Resource<>(status, null);
    }

    public static <T> Resource<T> initialLoading() {
        return new Resource<>(Status.INITIAL_LOADING, null);
    }

    public static <T> Resource<T> loading() {
        return new Resource<>(Status.LOADING, null);
    }

    @NonNull public Status getStatus() {
        return status;
    }

    public void setStatus(@NonNull Status status) {
        this.status = status;
    }

    @Nullable public T getData() {
        return data;
    }

    public void setData(@Nullable T data) {
        this.data = data;
    }

}
