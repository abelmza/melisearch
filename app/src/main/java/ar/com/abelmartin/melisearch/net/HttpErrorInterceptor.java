package ar.com.abelmartin.melisearch.net;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class HttpErrorInterceptor implements Interceptor {
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        if (response.code() >= 300 || response.code() < 200) {
            throw new GenericHttpException(response.code());
        }

        return response;
    }
}
