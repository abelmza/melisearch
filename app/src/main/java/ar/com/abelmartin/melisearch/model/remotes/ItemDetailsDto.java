package ar.com.abelmartin.melisearch.model.remotes;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class ItemDetailsDto {

    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("subtitle")
    private Object subtitle;
    @SerializedName("seller_id")
    private int sellerId;
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("official_store_id")
    private Object officialStoreId;
    @SerializedName("price")
    private double price;
    @SerializedName("base_price")
    private double basePrice;
    @SerializedName("original_price")
    private double originalPrice;
    @SerializedName("currency_id")
    private String currencyId;
    @SerializedName("sold_quantity")
    private int soldQuantity;
    @SerializedName("buying_mode")
    private String buyingMode;
    @SerializedName("listing_type_id")
    private String listingTypeId;
    @SerializedName("start_time")
    private Date startTime;
    @SerializedName("stop_time")
    private Date stopTime;
    @SerializedName("condition")
    private String condition;
    @SerializedName("permalink")
    private String permalink;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("secure_thumbnail")
    private String secureThumbnail;
    @SerializedName("pictures")
    private List<PictureDto> pictures = null;
    @SerializedName("descriptions")
    private List<DescriptionDto> descriptions = null;
    @SerializedName("accepts_mercadopago")
    private boolean acceptsMercadopago;
    @SerializedName("attributes")
    private List<AttributeDto> attributes = null;
    @SerializedName("parent_item_id")
    private String parentItemId;
    @SerializedName("date_created")
    private Date dateCreated;
    @SerializedName("last_updated")
    private Date lastUpdated;
    @SerializedName("health")
    private Object health;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(Object subtitle) {
        this.subtitle = subtitle;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Object getOfficialStoreId() {
        return officialStoreId;
    }

    public void setOfficialStoreId(Object officialStoreId) {
        this.officialStoreId = officialStoreId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public int getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(int soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public String getBuyingMode() {
        return buyingMode;
    }

    public void setBuyingMode(String buyingMode) {
        this.buyingMode = buyingMode;
    }

    public String getListingTypeId() {
        return listingTypeId;
    }

    public void setListingTypeId(String listingTypeId) {
        this.listingTypeId = listingTypeId;
    }


    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public List<PictureDto> getPictures() {
        return pictures;
    }

    public void setPictures(List<PictureDto> pictures) {
        this.pictures = pictures;
    }

    public List<DescriptionDto> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DescriptionDto> descriptions) {
        this.descriptions = descriptions;
    }

    public boolean isAcceptsMercadopago() {
        return acceptsMercadopago;
    }

    public void setAcceptsMercadopago(boolean acceptsMercadopago) {
        this.acceptsMercadopago = acceptsMercadopago;
    }

    public List<AttributeDto> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<AttributeDto> attributes) {
        this.attributes = attributes;
    }

    public String getParentItemId() {
        return parentItemId;
    }

    public void setParentItemId(String parentItemId) {
        this.parentItemId = parentItemId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Object getHealth() {
        return health;
    }

    public void setHealth(Object health) {
        this.health = health;
    }
}
