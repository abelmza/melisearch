package ar.com.abelmartin.melisearch.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

//required by Glide library (from v4) to allow placeholder images.
@GlideModule
public class MeliAppGlideModule extends AppGlideModule {}

