package ar.com.abelmartin.melisearch.ui.itemDetails;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ar.com.abelmartin.melisearch.R;
import ar.com.abelmartin.melisearch.model.Status;
import ar.com.abelmartin.melisearch.model.remotes.ItemDescriptionDto;
import ar.com.abelmartin.melisearch.model.remotes.ItemDetailsDto;
import ar.com.abelmartin.melisearch.utils.ui.BaseActivity;
import ar.com.abelmartin.melisearch.viewmodel.ItemDetailsViewModel;
import butterknife.BindView;
import butterknife.OnClick;
import timber.log.Timber;

public class ItemDetailsActivity extends BaseActivity {

    private final static String EXTRA_PRODUCT_ID = ItemDetailsActivity.class.getName() + ".PRODUCT_ID";

    @BindView(R.id.vpImages) ViewPager vpImages;
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.tvPrice) TextView tvPrice;
    @BindView(R.id.tvPriceDecimals) TextView tvPriceDecimals;
    @BindView(R.id.tvDescription) TextView tvDescription;
    @BindView(R.id.tvDiscount) TextView tvDiscount;

    @BindView(R.id.vBackground) View vBackground;
    @BindView(R.id.ivStatusImage) ImageView ivStatusImage;
    @BindView(R.id.tvMessage) TextView tvMessage;


    private ItemDetailsViewModel viewModel;
    private String itemId;

    // flag used only to handle item description error (not for all the details)
    private boolean errorOccured = false;

    //activity intent wrapper
    public static Intent getActivityIntent(Context context, String productId) {
        Intent i = new Intent(context, ItemDetailsActivity.class);
        i.putExtra(EXTRA_PRODUCT_ID, productId);
        return i;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Activity events
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        if (getIntent().getExtras() != null) {
            itemId = getIntent().getStringExtra(EXTRA_PRODUCT_ID);
        } else {
            throw new RuntimeException("itemId must be provided in " + ItemDetailsActivity.class.getSimpleName());
        }

        initializeViewModel();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // User interaction events
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @OnClick(R.id.tvDescription)
    void tvDescriptionClicked() {
        // allowing refetch only if error has occured before
        if (viewModel.getItemDescription().getValue() == null
                || viewModel.getItemDescription().getValue().getData() == null) {
            viewModel.refetchInfo();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private void initializeViewModel() {
        showInformationPanel(true);
        viewModel = ViewModelProviders.of(this).get(ItemDetailsViewModel.class);
        viewModel.fetchItemDetails(itemId).observe(this, (itemDetailsResource) -> {
            switch (itemDetailsResource.getStatus()) {
                case SUCCESS:
                    showInformationPanel(false);
                    showItemDetails(itemDetailsResource.getData());
                    break;
                case INITIAL_LOADING:
                case LOADING:
                    showInformationPanel(true, getString(R.string.please_wait), R.drawable.ic_update_black_150dp);
                    break;
                default:
                    showInformationPanel(false);
                    showError(itemDetailsResource.getStatus());
                    break;
            }
        });

        viewModel.getItemDescription().observe(this, (itemDescriptionResource) -> {
            switch (itemDescriptionResource.getStatus()) {
                case SUCCESS:
                    showItemDescription(itemDescriptionResource.getData());
                    break;
                case LOADING:
                    tvDescription.setText(R.string.please_wait);
                    break;
                default:
                    showDescriptionError(itemDescriptionResource.getStatus());
                    break;
            }

        });
    }

    private void showItemDetails(ItemDetailsDto itemDetails) {

        if (itemDetails.getPictures().size() > 0) {
            ImagesPagerAdapter adapter = new ImagesPagerAdapter(this, itemDetails.getPictures());
            vpImages.setAdapter(adapter);
        }

        tvTitle.setText(itemDetails.getTitle());
        tvPrice.setText(String.format(getString(R.string.price), itemDetails.getPrice()));

        //extract decimal and show decimal label ONLY when price has decimals.
        int decimal = (int) ((itemDetails.getPrice() - (int) itemDetails.getPrice()) * 100);
        if (decimal == 0) {
            tvPriceDecimals.setVisibility(View.GONE);
        } else {
            tvPriceDecimals.setVisibility(View.VISIBLE);
            tvPriceDecimals.setText(String.valueOf(decimal));
        }
        //showing discount only when needed
        if (itemDetails.getOriginalPrice() > itemDetails.getPrice()) {
            int discount = (int) (((itemDetails.getOriginalPrice() - itemDetails.getPrice()) * 100) / itemDetails.getOriginalPrice());
            tvDiscount.setText(getString(R.string.discount, discount));
            tvDiscount.setVisibility(View.VISIBLE);
        } else {
            tvDiscount.setVisibility(View.INVISIBLE);
        }

    }

    private void showError(Status status) {
        getDialogBuilder()
                .setMessage(getMessageForStatus(status))
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    finish();
                })
                .setCancelable(false)
                .show();
    }

    private void showItemDescription(ItemDescriptionDto itemDescription) {
        if (itemDescription.getPlainText() != null) {
            tvDescription.setText(itemDescription.getPlainText());
            tvDescription.setTag(null);
        }
    }

    private void showDescriptionError(Status status) {
        tvDescription.setText(R.string.error_generic_retry);
        tvDescription.setTag(status);
    }

    private void showInformationPanel(boolean show) {
        showInformationPanel(show, null, 0);
    }

    private void showInformationPanel(boolean show, String message, int imageId) {
        if (message != null) {
            tvMessage.setText(message);
        }

        if (imageId != 0) {
            ivStatusImage.setImageResource(imageId);
        }

        vBackground.setVisibility(show ? View.VISIBLE : View.GONE);
        tvMessage.setVisibility(show ? View.VISIBLE : View.GONE);
        ivStatusImage.setVisibility(show ? View.VISIBLE : View.GONE);
    }

}
