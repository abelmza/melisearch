package ar.com.abelmartin.melisearch.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import ar.com.abelmartin.melisearch.model.Resource;
import ar.com.abelmartin.melisearch.model.remotes.ItemDescriptionDto;
import ar.com.abelmartin.melisearch.model.remotes.ItemDetailsDto;
import ar.com.abelmartin.melisearch.model.repositories.ItemDetailsRepository;

public class ItemDetailsViewModel extends ViewModel {

    private ItemDetailsRepository repository;


    private MutableLiveData<String> searchItemId = new MutableLiveData<>();

    //using transformations to avoid callback-to-livedata memory leaks
    private LiveData<Resource<ItemDetailsDto>> itemDetails
            = Transformations.switchMap(searchItemId, (itemId) -> repository.getItemDetails(itemId));

    private LiveData<Resource<ItemDescriptionDto>> itemDescription
            = Transformations.switchMap(searchItemId, (itemId) -> repository.getItemDescription(itemId));

    public ItemDetailsViewModel() {
        repository = new ItemDetailsRepository();
    }

    public LiveData<Resource<ItemDetailsDto>> fetchItemDetails(String itemId) {
        searchItemId.setValue(itemId);
        return itemDetails;
    }

    public LiveData<Resource<ItemDescriptionDto>> getItemDescription() {
        return itemDescription;
    }

    /**
     * Refetchs info from repository. Can be used after getting an error
     */
    public void refetchInfo() {
        searchItemId.setValue(searchItemId.getValue());
    }
}
