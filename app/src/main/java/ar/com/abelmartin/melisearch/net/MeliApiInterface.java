package ar.com.abelmartin.melisearch.net;

import ar.com.abelmartin.melisearch.model.remotes.ItemDescriptionDto;
import ar.com.abelmartin.melisearch.model.remotes.ItemDetailsDto;
import ar.com.abelmartin.melisearch.model.remotes.SearchResultsDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MeliApiInterface {
    @GET("/sites/MLA/search")
    Call<SearchResultsDto> searchItems(@Query("q") String searchQuery,
                                       @Query("limit") int limit,
                                       @Query("offset") int offset);

    @GET("/items/{itemId}")
    Call<ItemDetailsDto> getItemDetails(@Path("itemId") String itemId);

    @GET("/items/{itemId}/description")
    Call<ItemDescriptionDto> getItemDescription(@Path("itemId") String itemId);
}
