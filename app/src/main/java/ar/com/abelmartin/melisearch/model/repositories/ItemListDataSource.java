package ar.com.abelmartin.melisearch.model.repositories;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PositionalDataSource;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import ar.com.abelmartin.melisearch.managers.NetworkManager;
import ar.com.abelmartin.melisearch.model.Status;
import ar.com.abelmartin.melisearch.model.remotes.ItemDto;
import ar.com.abelmartin.melisearch.net.NetworkError;
import ar.com.abelmartin.melisearch.utils.ErrorUtils;


public class ItemListDataSource extends PositionalDataSource<ItemDto> {
    // Extending PositionalDataSource as it fits the way that MercadoLibre's API works
    // It works with positions and page sizes, just as ML API -- it asks for page_size (limit)
    // and item_offset which can be calculated as (page_number * page_size) --

    private MutableLiveData<Status> statusHandler = new MutableLiveData<>();
    private String searchQuery;

    public ItemListDataSource(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    @Override public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<ItemDto> callback) {

        statusHandler.postValue(Status.INITIAL_LOADING);

        NetworkManager.getInstance().searchItems(searchQuery, params.requestedLoadSize, 0, (response, error) -> {
            if (postNetworkStatus(error)) {
                callback.onResult(new ArrayList<>(), 0, 0);
            } else {
                callback.onResult(response.getResults(), 0, response.getPaging().getTotal());
            }
        });

    }

    @Override public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<ItemDto> callback) {
        statusHandler.postValue(Status.LOADING);

        NetworkManager.getInstance().searchItems(searchQuery, params.loadSize, params.startPosition, (response, error) -> {
            if (!postNetworkStatus(error)) {
                callback.onResult(response.getResults());
            }
        });
    }

    public MutableLiveData<Status> getStatusHandler() {
        return statusHandler;
    }

    /**
     * Returns true if error != null and error status is dispatched
     *
     * @param error result of latest network call
     * @return
     */
    private boolean postNetworkStatus(@Nullable NetworkError error) {
        if (error != null) {
            statusHandler.postValue(ErrorUtils.from(error));
            return true;
        } else {
            statusHandler.postValue(Status.SUCCESS);
            return false;
        }
    }
}
