package ar.com.abelmartin.melisearch.ui.itemDetails;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import ar.com.abelmartin.melisearch.R;
import ar.com.abelmartin.melisearch.model.remotes.PictureDto;
import ar.com.abelmartin.melisearch.utils.GlideApp;

public class ImagesPagerAdapter extends PagerAdapter {

    LayoutInflater layoutInflater;
    private List<PictureDto> pictures;

    public ImagesPagerAdapter(Context context, List<PictureDto> pictures) {
        super();
        this.pictures = pictures;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override public int getCount() {
        return pictures.size();
    }

    @Override public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull @Override public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.page_image, container, false);
        ImageView imageView = itemView.findViewById(R.id.ivPicture);
        container.addView(itemView);

        PictureDto currentPicture = pictures.get(position);
        GlideApp.with(itemView).load(currentPicture.getUrl())
                .placeholder(R.drawable.ic_image_black_120dp)
                .into(imageView);

        return itemView;
    }

    @Override public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
