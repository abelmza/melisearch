package ar.com.abelmartin.melisearch.model.remotes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResultsDto {
    @SerializedName("site_id")
    private String siteId;

    @SerializedName("query")
    private String query;

    @SerializedName("paging")
    private PagingInfoDto paging;

    @SerializedName("results")
    private List<ItemDto> results = null;

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public PagingInfoDto getPaging() {
        return paging;
    }

    public void setPaging(PagingInfoDto paging) {
        this.paging = paging;
    }

    public List<ItemDto> getResults() {
        return results;
    }

    public void setResults(List<ItemDto> results) {
        this.results = results;
    }
}
