package ar.com.abelmartin.melisearch.ui.main;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import ar.com.abelmartin.melisearch.R;
import ar.com.abelmartin.melisearch.model.remotes.ItemDto;
import ar.com.abelmartin.melisearch.utils.GlideApp;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class ItemListAdapter extends PagedListAdapter<ItemDto, ItemListAdapter.ViewHolder> {

    private ItemSelectedListener itemSelectedListener;

    public ItemListAdapter(ItemSelectedListener itemSelectedListener) {
        super(ItemDto.DIFF_CALLBACK);
        this.itemSelectedListener = itemSelectedListener;
    }

    @NonNull @Override public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);
        return new ViewHolder(itemView, itemSelectedListener);
    }

    @Override public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemDto item = getItem(position);
        if (item != null) {
            holder.showItem(item);
        }
    }

    public interface ItemSelectedListener {
        void onItemSelected(ItemDto item);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ItemDto item;
        @BindView(R.id.ivThumbnail) ImageView ivThumbnail;
        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.tvPrice) TextView tvPrice;
        @BindView(R.id.tvPriceDecimals) TextView tvPriceDecimal;

        private Context context;
        private ItemSelectedListener itemSelectedListener;

        public ViewHolder(View v, ItemSelectedListener itemSelectedListener) {
            super(v);
            ButterKnife.bind(this, v);
            context = v.getContext();
            this.itemSelectedListener = itemSelectedListener;
        }

        public void showItem(ItemDto item) {
            this.item = item;
            GlideApp.with(ivThumbnail)
                    .load(item.getThumbnail())
                    .placeholder(R.drawable.ic_image_black_120dp)
                    .into(ivThumbnail);

            tvTitle.setText(item.getTitle());

            tvPrice.setText(String.format(Locale.getDefault(), context.getString(R.string.price), item.getPrice()));

            //extract decimal and show decimal label ONLY when price has decimals.
            int decimal = (int) ((item.getPrice() - (int) item.getPrice()) * 100);
            if (decimal == 0) {
                tvPriceDecimal.setVisibility(View.INVISIBLE);
            } else {
                tvPriceDecimal.setVisibility(View.VISIBLE);
                tvPriceDecimal.setText(String.valueOf(decimal));
            }
        }

        @OnClick(R.id.clMain)
        void onItemClicked() {
            if (itemSelectedListener != null) {
                itemSelectedListener.onItemSelected(item);
            }
        }
    }

}
