package ar.com.abelmartin.melisearch.model.remotes;

import android.support.v7.util.DiffUtil;

import com.google.gson.annotations.SerializedName;

import java.util.Locale;

public class ItemDto {

    //DIFF_CALLBACK is needed used in PagedListAdapter
    public static DiffUtil.ItemCallback<ItemDto> DIFF_CALLBACK = new DiffUtil.ItemCallback<ItemDto>() {
        @Override public boolean areItemsTheSame(ItemDto oldItem, ItemDto newItem) {
            return oldItem.id.equals(newItem.id);
        }

        //comparing only most critical values
        @Override public boolean areContentsTheSame(ItemDto oldItem, ItemDto newItem) {
            return oldItem.id.equals(newItem.id)
                    && oldItem.title.equals(newItem.title)
                    && oldItem.price == newItem.price;
        }
    };


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Properties from JSON
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("price")
    private double price;
    @SerializedName("currency_id")
    private String currencyId;
    @SerializedName("available_quantity")
    private double availableQuantity;
    @SerializedName("sold_quantity")
    private double soldQuantity;
    @SerializedName("condition")
    private String condition;
    @SerializedName("permalink")
    private String permalink;
    @SerializedName("thumbnail")
    private String thumbnail;
    @SerializedName("accepts_mercadopago")
    private Boolean acceptsMercadopago;
    @SerializedName("original_price")
    private double originalPrice;
    @SerializedName("category_id")
    private String categoryId;
    @SerializedName("official_store_id")
    private Integer officialStoreId;
    @SerializedName("catalog_product_id")
    private String catalogProductId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public double getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(double availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public double getSoldQuantity() {
        return soldQuantity;
    }

    public void setSoldQuantity(double soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Boolean getAcceptsMercadopago() {
        return acceptsMercadopago;
    }

    public void setAcceptsMercadopago(Boolean acceptsMercadopago) {
        this.acceptsMercadopago = acceptsMercadopago;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getOfficialStoreId() {
        return officialStoreId;
    }

    public void setOfficialStoreId(Integer officialStoreId) {
        this.officialStoreId = officialStoreId;
    }

    public String getCatalogProductId() {
        return catalogProductId;
    }

    public void setCatalogProductId(String catalogProductId) {
        this.catalogProductId = catalogProductId;
    }

    @Override public String toString() {
        return String.format(Locale.US, "id: %s, title: %s, price: %f", id, title, price);
    }

    @Override public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof ItemDto)) {
            return false;
        } else {
            ItemDto other = (ItemDto) obj;
            return other.id.equals(id)
                    && other.title.equals(title)
                    && other.price == price;
        }
    }
}
