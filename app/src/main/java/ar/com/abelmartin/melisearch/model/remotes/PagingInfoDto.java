package ar.com.abelmartin.melisearch.model.remotes;

import com.google.gson.annotations.SerializedName;

public class PagingInfoDto {

    @SerializedName("total")
    private int total;

    @SerializedName("offset")
    private int offset;

    @SerializedName("limit")
    private int limit;

    @SerializedName("primary_results")
    private int primaryResults;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPrimaryResults() {
        return primaryResults;
    }

    public void setPrimaryResults(int primaryResults) {
        this.primaryResults = primaryResults;
    }
}
