# MeliSearch

## Description
Basic Android app that allows users to perform basic search operations using MercadoLibre's public API. 

## Implementation details
The main idea was to follow Google's latest architectural suggestions and patterns by implementing Android Jetpack's Architecture components

### Arquitecture
MVVM on the presentation layer. Related concepts: Clean arquitecture

#### Related design patterns
Observer, Repository, Separation of concerns, singleton, and others.

### Libraries
  * Android Architecture
    * LiveData
    * Paging
    * ViewModel
  * Networking
    * Retrofit
    * Glide
  * View binding
    * Butterknife
  * Utilities
    * Timber
 
 Developed by Abel Martin (abelmza@gmail.com)